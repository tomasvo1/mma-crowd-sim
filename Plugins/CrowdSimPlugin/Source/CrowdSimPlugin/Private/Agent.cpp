// Fill out your copyright notice in the Description page of Project Settings.


#include "Agent.h"
#include "BuildingTile.h"


const float sizeScale = 1.0 / 1000.0;

Agent::Agent(BuildingTile * home, FVector wo, AgentSettings& settings)
{
	pos = home->getLocation();
	worldOffset = wo;

	transform.SetTranslation(pos + worldOffset);
	vel = FVector(0);
	acc = FVector(0);

	
	homeTile = home;
	foodTile = nullptr;

	size = settings.size;
	radius = settings.radius;
	personalSpaceEpsilon = settings.personalSpace;
	pushingLambda = settings.pushing;
	walkTolerance = settings.walkCheckpointTolerance;
	hungryLevel = settings.hungryLevel;

	foodCapacity = settings.foodCapacity;
	foodHungerDelta = settings.foodHungerDelta;
	foodEatingDelta = settings.foodEatingDelta;
	patienceLimit = settings.patienceLimit;
	patienceDelta = settings.patienceDelta;
	talkingDistance = settings.talkingDistance;

	weightWallRepulsion = settings.weightWallRepulsion;
	weightAgentRepulsion = settings.weightAgentRepulsion;
	weightWallAvoidance = settings.weightWallAvoidance;
	weightAgentAvoidance = settings.weightAgentAvoidance;
	weightSteeringForce = settings.weightSteeringForce;

	patience = patienceLimit;
	food = foodCapacity / 10;
	stopCounter = 0;
	//alpha init todo
	status = AgentState::home;
	
	transform.SetScale3D(FVector(size * sizeScale)); // scale the ball down
}

Agent::~Agent()
{
}

void Agent::updateStatus(CityMap* map, std::vector<Agent*>& others)
{
	if (status != AgentState::eating)
		food = std::max(0.0f, food - foodHungerDelta);

	switch (status)
	{
	case home:
		if (food < hungryLevel)
			if (moveToStreet(map))
				goFood(map);
		break;
	case searching:
		if (walk.endReached())
			goToFoodmarketDoor(map);
		break;
	case waiting:
		if (isGoingToWait(map))
			if (walk.endReached()) //try walk towards door
				if (tryGetInsideFoodmarket(map))
					startEat(map);
				else
					waitAtFoodmarketDoor(map);
		break;
	case walkingAround:
		if (!newMarketDiscovered(map, others)) 
			if (walk.endReached())
				planRandomWalk(map);
		//else if new is discovered, goes to searching
		break;
	case eating:
		if (food < foodCapacity)
			eat(map);
		else if (moveToStreet(map))
			goHome(map);
		break;
	case returning:
		if (walk.endReached())
			goToHomeDoor(map);
		break;
	case goingToHomeDoor:
		if (walk.endReached())
			stayHome(map);
		break;
	default:
		break;
	}

	updateWaitInterval();
}

bool Agent::moveToStreet(CityMap* map)
{
	BuildingTile * curtile = (BuildingTile*) map->getTileForLocation(pos);
	CityTile * street = map->getNearesetStreetTile(map->getTileForLocation(pos));

	if (street)
	{
		FVector door = curtile->doorLocation(street);
		pos = door;
		return true;
	}

	return false;
}


bool Agent::goFood(CityMap* map)
{
	foodTile = (FoodTile*) map->getNearesetFoodTile(map->getTileForLocation(pos));
	if (!foodTile)
	{
		//todo backup plan if there is no food
		return false;
	}

	CityTile * streetInFrontFood = map->getNearesetStreetTile(foodTile);
	if (!streetInFrontFood)
	{
		return false;
	}
	
	walk = Walk(pos, streetInFrontFood->getLocation(), map, walkTolerance);
	status = AgentState::searching;

	return true;
}

bool Agent::goToFoodmarketDoor(CityMap* map)
{

	CityTile* current = map->getTileForLocation(pos);
	FVector inside = foodTile->doorLocation(current);
	walk = Walk(pos, inside, map, walkTolerance);
	status = AgentState::waiting;
	return true;
}

bool Agent::tryGetInsideFoodmarket(CityMap* map)
{
	return foodTile->tryAgentGetInside();
}


bool Agent::waitAtFoodmarketDoor(CityMap* map)
{
	CityTile* current = map->getTileForLocation(pos);
	FVector inside = foodTile->doorLocation(current);
	walk = Walk(pos, inside, walkTolerance);
	return true;
}

bool Agent::isGoingToWait(CityMap* map)
{
	patience = std::max(0.0f, patience - patienceDelta);

	if (patience < 0.5)
	{
		planRandomWalk(map);
		patience = patienceLimit;
		return false;
	}

	return true;
}

bool Agent::planRandomWalk(CityMap* map)
{
	status = AgentState::walkingAround;
	CityTile* tile = map->getRandomStreetTile();
	walk = Walk(pos, tile->getLocation(), map, walkTolerance);

	return true;
}

bool Agent::newMarketDiscovered(CityMap* map, std::vector<Agent*>& others)
{
	for (const auto& o : others)
	{
		if (o->isStopped() || dist(o) > talkingDistance)
			continue;

		if (o->foodTile != foodTile)
		{
			foodTile = o->foodTile;
			CityTile* streetInFrontFood = map->getNearesetStreetTile(foodTile);
			if (!streetInFrontFood)
			{
				return false;
			}

			walk = Walk(pos, streetInFrontFood->getLocation(), map, walkTolerance);
			status = AgentState::searching;

			stopCounter = map->getRandomWaitInterval();
			o->stopCounter = stopCounter;

			return true;
		}
	}

	return false;
}

bool Agent::startEat(CityMap* map) 
{
	pos = foodTile->getInsideAgentPosition(this);
	status = AgentState::eating;
	return true;
}

bool Agent::eat(CityMap* map)
{
	food += foodEatingDelta;
	return true;
}

bool Agent::goHome(CityMap* map)
{
	foodTile->agentGetOut(this);
	CityTile* streetInFrontHome = map->getNearesetStreetTile(homeTile);
	walk = Walk(pos, streetInFrontHome->getLocation(), map, walkTolerance);
	status = AgentState::returning;
	return true;
}

bool Agent::goToHomeDoor(CityMap* map)
{
	CityTile* current = map->getTileForLocation(pos);
	FVector inside = homeTile->doorLocation(current);
	walk = Walk(pos, inside, map, walkTolerance);
	status = AgentState::goingToHomeDoor;
	return true;
}

bool Agent::stayHome(CityMap* map)
{
	pos = homeTile->getLocation();
	status = AgentState::home;
	return true;
}

FVector Agent::otherAgentsRepulsionForce(std::vector<Agent*>& others) const {
	FVector dir;
	float dist, distAttr;
	FVector force = FVector(0);

	for (const auto& o : others)
	{
		dir = pos - o->pos;
		dist = dir.Size();

		distAttr = radius + personalSpaceEpsilon + o->radius - dist;
		if (distAttr > 0 && dist > 0)
			force += (dir * distAttr) / dist;
	}

	return force;
}

FVector Agent::otherAgentsAvoidanceForce(std::vector<Agent*>& others) const {
	FVector dir;
	float dist, distAttr, wi = 0, wd;
	FVector force = FVector(0), aforce;

	for (const auto& o : others)
	{
		dir = pos - o->pos;
		dist = dir.Size();

		distAttr = radius + personalSpaceEpsilon + o->radius - dist;
		if (distAttr > 0 && dist > 0)
		{
			aforce = FVector::CrossProduct(FVector::CrossProduct(dir, vel), dir).GetSafeNormal();

			wi = distAttr * distAttr;
			wd = FVector::DotProduct(vel, o->vel) > 0 ? 1.2 : 2.4;

			force += aforce * wi * wd;
		}
	}

	return force;
}

void Agent::forcesCompute(CityMap* map, std::vector<Agent*>& others)
{
	updateStatus(map, others);

	//avoidances
	avoidanceWallForce = map->wallsAvoidanceForce(pos, vel, radius, personalSpaceEpsilon).GetSafeNormal();
	avoidanceAgentsForce = otherAgentsAvoidanceForce(others);

	//repulsions
	repulsionWallForce = map->wallsRepulsionForce(pos, radius, personalSpaceEpsilon);
	repulsionAgentsForce = otherAgentsRepulsionForce(others).GetSafeNormal();
}

void Agent::checkStoppingRule()
{
	alpha = (repulsion.SizeSquared() > 0) && (FVector::DotProduct(vel, repulsionAgentsForce) < 0) ? 0 : 1;
}

void Agent::updateWaitInterval()
{
	stopCounter = std::max(0, stopCounter - 1);
}

float Agent::dist(Agent* agent) const
{
	return (pos - agent->pos).Size();
}

int Agent::isNotLookingForOthers() const
{
	return status == AgentState::walkingAround ? 0 : 1;
}

bool Agent::isStopped() const
{
	return stopCounter > 0;
}

void Agent::forcesApply(float deltaTime)
{
	float z = pos.Z;

	//apply forces
	if (walk.getForce(pos, attractorForce) == WalkStatus::walking
		&& stopCounter == 0)
	{
		steering = weightSteeringForce * attractorForce
			+ weightWallAvoidance * avoidanceWallForce
		    + weightAgentAvoidance * avoidanceAgentsForce * isNotLookingForOthers();

		steering = steering.GetSafeNormal();

		repulsion = repulsionWallForce * weightWallRepulsion
			+ weightAgentRepulsion * pushingLambda * repulsionAgentsForce * (0.5 + 0.5 * isNotLookingForOthers());

		repulsion = repulsion.GetSafeNormal();
		
		checkStoppingRule();

		acc = alpha * steering + repulsion;

		vel += acc.GetClampedToMaxSize(1.0);
		vel = vel.GetClampedToMaxSize(1.0) * std::min(deltaTime, 1.0f); //to prevent large steps
		pos += vel;
		pos.Z = z; //to fix floating
	}
	
	transform.SetTranslation(pos + worldOffset);
}

FVector Agent::getPosition() const
{
	return pos;
}

float Agent::getFeatureState() const
{
	if (stopCounter)
		return 7.1;

	switch (status)
	{
	case home:
		return 0.1;
		break;
	case searching:
		return 1.1;
		break;
	case waiting:
		return 2.1;
		break;
	case walkingAround:
		return 3.1;
		break;
	case eating:
		return 4.1;
		break;
	case returning:
		return 5.1;
		break;
	case goingToHomeDoor:
		return 6.1;
		break;
	default:
		break;
	}

	return 0.1;
}