// Fill out your copyright notice in the Description page of Project Settings.


// Fill out your copyright notice in the Description page of Project Settings.


#include "BorderTile.h"

BorderTile::BorderTile(int _x, int _y, int _w, int _h, int _z)
	: CityTile(_x, _y, _w, _h, _z) {}

int BorderTile::generate_mesh(TArray<FVector>& vertices, TArray<FVector>& normals, TArray<int32>& indices, TArray<FVector2D>& uvs, const int index_offset, TArray<FLinearColor>& color)
{
	int count = generateBox(vertices, normals, indices, uvs, index_offset, *this);

	for (int i = 0; i < count; ++i)
		color.Add(FLinearColor(1.0, 1.0, 1.0, 1.0));

	return count;
}

int BorderTile::getTileType()
{
	return TileType::Border;
}
