// Fill out your copyright notice in the Description page of Project Settings.


#include "BuildingTile.h"

BuildingTile::BuildingTile(int _x, int _y, int _w, int _h, int _z)
	: CityTile(_x, _y, _w, _h, _z) {}

int BuildingTile::generate_mesh(TArray<FVector>& vertices, TArray<FVector>& normals, TArray<int32>& indices, TArray<FVector2D>& uvs, const int index_offset, TArray<FLinearColor> & color)
{
	int count = generateBox(vertices, normals, indices, uvs, index_offset, *this);
	
	for(int i = 0; i < count; ++i)
		color.Add(FLinearColor(1.0, 1.0, 1.0, 1.0));

	return count;
}

FVector BuildingTile::nearestWallNormal(const FVector & pos) const
{
	float dx = std::min(pos.X - pos_x, 0.0f) + std::max(0.0f, pos.X - (pos_x + w));
	float dy = std::min(pos.Y - pos_y, 0.0f) + std::max(0.0f, pos.Y - (pos_y + h));

	//report normal only for 4-neighborhood
	if (dx != 0 && dy != 0)
		return FVector(0);

	return FVector(dx, dy, 0).GetSafeNormal();
}

int BuildingTile::getTileType()
{
	return TileType::Building;
}


FVector BuildingTile::doorLocation(CityTile* street)
{
	FVector streetLoc = street->getLocation();
	FVector insideLoc = getLocation();
	insideLoc.Z = streetLoc.Z;

	FVector dir = insideLoc - streetLoc;
	dir = dir.GetClampedToMaxSize(std::min(w, h) / 2.0) * 0.6;

	return streetLoc + dir;
}
