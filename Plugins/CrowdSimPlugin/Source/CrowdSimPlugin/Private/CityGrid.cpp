// Fill out your copyright notice in the Description page of Project Settings.


#include "CityGrid.h"
#include "CitySettings.h"

#include <vector>
#include <random>

// Sets default values
ACityGrid::ACityGrid()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Generate base geometry
	mesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("CityMesh"));
	static ConstructorHelpers::FObjectFinder<UMaterial> MaterialOb(TEXT("Material'/CrowdSimPlugin/VertexMaterial.VertexMaterial'")); 
	material = MaterialOb.Object;
	
	// All transformations are applied to this mesh
	RootComponent = mesh;

	//generate city
	speed = 5;
	height = 20;
	spacing = 20;
	layout = nullptr;
	generateMesh = false;
	displayCustomersInRow = 3;
	limitCustomersInside = 9;

	//individual
	personalSpaceRangeMin = 1.0;
	personalSpaceRangeMax = 5.0;
	pushFactorRangeMin = 0.0;
	pushFactorRangeMax = 2.0;
	foodHungerDeltaMin = 0.1;
	foodHungerDeltaMax = 0.1;
	foodEatingDeltaMin = 1.0;
	foodEatingDeltaMax = 1.0;
	foodCapacityMin = 100.0;
	foodCapacityMax = 100.0;
	patienceLimitMin = 1000.0;
	patienceLimitMax = 1000.0;
	patienceDeltaMin = 0.5;
	patienceDeltaMax = 0.5;

	//common
	perceptionRadius = 15;
	walkCheckpointTolerance = 4;
	agentRadius = 1;
	talkingDistance = 4;
	weightWallRepulsion = 1.0;
	weightAgentRepulsion = 1.0;
	weightWallAvoidance = 1.0;
	weightAgentAvoidance = 1.0;
	weightSteeringForce = 10.0;
	hungryLevel = 5;

	//initiate agents mesh
	agentMesh = CreateDefaultSubobject<UInstancedStaticMeshComponent>(TEXT("AgentMesh"));
	agentMesh->NumCustomDataFloats = 1;
	agentMesh->RegisterComponent();

	static ConstructorHelpers::FObjectFinder<UMaterial> MaterialAgentOb(TEXT("Material'/CrowdSimPlugin/AgentMaterial.AgentMaterial'"));
	agentMaterial = MaterialAgentOb.Object;

	agentMeshInstance = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Engine/BasicShapes/Sphere.Sphere'")).Object;

	agentMeshInstance->SetMaterial(0, agentMaterial);

	agentMesh->SetStaticMesh(agentMeshInstance);

	map = nullptr;
}

ACityGrid::~ACityGrid()
{
	if (map)
		delete map;
}

// Called when the game starts or when spawned
void ACityGrid::BeginPlay()
{
	Super::BeginPlay();

	initCity();
	initAgents();
}

// Called every frame
void ACityGrid::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	std::vector<Agent*> others;

	//update agents, compute forces
	for (size_t i = 0; i < agents.size(); ++i)
	{
		map->getOccupantsForPosition(others, agents[i]->getPosition(), perceptionRadius);
		agents[i]->forcesCompute(map, others);
		others.clear();
	}

	
	//reset occupants
	map->emptyOccupants();

	FVector loc;
	CityTile* tile;

	//update agent geometry position
	for (size_t i = 0; i < agents.size(); ++i)
	{
		agents[i]->forcesApply(DeltaTime * speed * 10.0f);
		agentMesh->UpdateInstanceTransform(i, agents[i]->transform, true, i == (agents.size() - 1));
		agentMesh->SetCustomDataValue(i, 0, agents[i]->getFeatureState(), i == (agents.size() - 1));

		loc = agents[i]->getPosition();
		tile = map->getTileForLocation(loc);


		//to prevent all evil
		if (tile)
			tile->addOccupant(agents[i]);
		else
		{
			agents.erase(agents.begin() + i);
			i--;
		}
	}
}

void clampMinMax(float& min, float& max)
{
	min = std::min(min, max);
	max = std::max(min, max);
}

void ACityGrid::OnConstruction(const FTransform& Transform)
{
	if (generateMesh && layout)
		initCity();

	//clamp parameters
	clampMinMax(personalSpaceRangeMin, personalSpaceRangeMax);
	clampMinMax(pushFactorRangeMin, pushFactorRangeMax);
	clampMinMax(foodHungerDeltaMin, foodHungerDeltaMax);
	clampMinMax(foodEatingDeltaMin, foodEatingDeltaMax);
	clampMinMax(foodCapacityMin, foodCapacityMax);
	clampMinMax(patienceLimitMin, patienceLimitMax);
	clampMinMax(patienceDeltaMin, patienceDeltaMax);
}

void ACityGrid::initCity()
{
	generateMesh = false;
	if (map)
		delete map;

	CitySettings settings;
	settings.spacing = spacing;
	settings.height = height;
	settings.material = material;
	settings.layout = layout;
	settings.mesh = mesh;
	settings.foodmarketAgentsRow = displayCustomersInRow;
	settings.foodmarketLimit = limitCustomersInside;

	map = new CityMap();
	map->generate(settings);
}

void ACityGrid::initAgents()
{

	std::default_random_engine generator;
	std::uniform_real_distribution<float> personalSpace(personalSpaceRangeMin, personalSpaceRangeMax);
	std::uniform_real_distribution<float> push(pushFactorRangeMin, pushFactorRangeMax);
	std::uniform_real_distribution<float> hunger(foodHungerDeltaMin, foodHungerDeltaMax);
	std::uniform_real_distribution<float> eat(foodEatingDeltaMin, foodEatingDeltaMax);
	std::uniform_real_distribution<float> limit(foodCapacityMin, foodCapacityMax);
	std::uniform_real_distribution<float> patienceLimit(patienceLimitMin, patienceLimitMax);
	std::uniform_real_distribution<float> patienceDelta(patienceDeltaMin, patienceDeltaMax);

	AgentSettings settings;
	settings.size = spacing;
	settings.radius = agentRadius;
	settings.hungryLevel = hungryLevel;
	settings.walkCheckpointTolerance = walkCheckpointTolerance;
	settings.talkingDistance = talkingDistance;
	settings.weightWallRepulsion = weightWallRepulsion;
	settings.weightAgentRepulsion = weightAgentRepulsion;
	settings.weightWallAvoidance = weightWallAvoidance;
	settings.weightAgentAvoidance = weightAgentAvoidance;
	settings.weightSteeringForce = weightSteeringForce;

	auto t = GetTransform();
	FVector woffset = t.GetLocation();

	const std::vector<CityTile*>& buildings = map->getBuildings();


	Agent* agent;
	for (size_t i = 0; i < buildings.size(); ++i)
	{
		settings.personalSpace = personalSpace(generator);
		settings.pushing = push(generator);
		settings.foodCapacity = limit(generator);
		settings.foodHungerDelta = hunger(generator);
		settings.foodEatingDelta = eat(generator);
		settings.patienceLimit = patienceLimit(generator);
		settings.patienceDelta = patienceDelta(generator);


		agent = new Agent((BuildingTile*) buildings[i], t.GetLocation(), settings);
		agentMesh->AddInstance(agent->transform);
		agents.push_back(agent);
	}
}