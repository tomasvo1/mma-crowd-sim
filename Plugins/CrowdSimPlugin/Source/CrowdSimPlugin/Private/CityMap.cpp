// Fill out your copyright notice in the Description page of Project Settings.



#include "CityMap.h"
#include "CityTile.h"
#include "BuildingTile.h"
#include "BorderTile.h"
#include "StreetTile.h"
#include "FoodTile.h"

#include <queue>
#include <vector>
#include <unordered_set>

CityMap::CityMap() 
{
	randomWaitInterval = std::uniform_int_distribution<int>(100, 500);
}

CityMap::~CityMap()
{
	for (size_t i = 0; i < res; ++i)
		delete tiles[i];
}

void CityMap::getStreetLocations(std::vector<FVector>& locations) const
{
	for (size_t i = 0; i < tiles.size(); ++i)
		if (tiles[i]->getTileType() == TileType::Street)
			locations.push_back(tiles[i]->getLocation());
}

void CityMap::getFoodLocations(std::vector<FVector>& locations) const
{
	for (size_t i = 0; i < tiles.size(); ++i)
		if (tiles[i]->getTileType() == TileType::Food)
			locations.push_back(tiles[i]->getLocation());
}

void CityMap::getBuildingLocations(std::vector<FVector>& locations) const
{
	for (size_t i = 0; i < tiles.size(); ++i)
		if (tiles[i]->getTileType() == TileType::Building)
			locations.push_back(tiles[i]->getLocation());
}

const std::vector<CityTile*>& CityMap::getBuildings() const
{
	return building;
}


CityTile* CityMap::getTileForLocation(const FVector loc) const
{

	int32 x = loc.X / spacing;
	int32 y = loc.Y / spacing;

	size_t idx = y * resolution_x + x;

	if (idx >= res || loc.X < 0 || loc.Y < 0)
		return nullptr;

	return tiles[idx];
}

CityTile* CityMap::getRandomStreetTile()
{
	int idx = randomStreet(generator);
	return street[idx];
}

int CityMap::getRandomWaitInterval()
{
	return randomWaitInterval(generator);
}

void CityMap::generate(const CitySettings& settings)
{
	spacing = settings.spacing;
	height = settings.height;
	generateCity(settings);
	generateMesh(settings);
}

void CityMap::generateCity(const CitySettings& settings)
{

	TextureCompressionSettings OldCompressionSettings = settings.layout->CompressionSettings;
	TextureMipGenSettings OldMipGenSettings = settings.layout->MipGenSettings;
	bool OldSRGB = settings.layout->SRGB;

	settings.layout->CompressionSettings = TextureCompressionSettings::TC_VectorDisplacementmap;
	settings.layout->MipGenSettings = TextureMipGenSettings::TMGS_NoMipmaps;
	settings.layout->SRGB = false;
	settings.layout->UpdateResource();


	//prepare texture
	const FColor* FormatedImageData = static_cast<const FColor*>(settings.layout->PlatformData->Mips[0].BulkData.LockReadOnly());

	resolution_x = settings.layout->PlatformData->Mips[0].SizeX;
	resolution_y = settings.layout->PlatformData->Mips[0].SizeY;

	for (int32 y = 0; y < resolution_y + 2; y++)
	{
		for (int32 x = 0; x < resolution_x + 2; x++)
		{
			if (x == 0 || y == 0 || x == (resolution_x + 1) || y == (resolution_y + 1))
			{
				tiles.push_back(new BorderTile(x, y, spacing, spacing, 1));
				continue;
			}

			FColor PixelColor = FormatedImageData[(y - 1) * resolution_x + (x - 1)];
			float r = PixelColor.R;
			float g = PixelColor.G;

			if (r != g)
			{
				tiles.push_back(new FoodTile(x, y, spacing, spacing, r / 255.f * height, settings.foodmarketLimit, settings.foodmarketAgentsRow));
				food.push_back(tiles.back());
			}
			else if (r == 0)
			{
				tiles.push_back(new StreetTile(x, y, spacing, spacing));
				street.push_back(tiles.back());
			}
			else
			{
				tiles.push_back(new BuildingTile(x, y, spacing, spacing, r / 255.f * height));
				building.push_back(tiles.back());
			}

		}
	}

	resolution_x += 2;
	resolution_y += 2;
	res = (resolution_x) * (resolution_y);

	settings.layout->PlatformData->Mips[0].BulkData.Unlock();
	settings.layout->CompressionSettings = OldCompressionSettings;
	settings.layout->MipGenSettings = OldMipGenSettings;
	settings.layout->SRGB = OldSRGB;
	settings.layout->UpdateResource();

	randomStreet = std::uniform_int_distribution<int>(0, street.size() - 1);

	for (size_t i = 0; i < res; ++i)
		tiles[i]->setup_neighbors(tiles, resolution_x, resolution_y);
}

void CityMap::generateMesh(const CitySettings& settings)
{
	//mesh arrays
	TArray<FVector> vertices;
	TArray<FVector> normals;
	TArray<int32> indices;
	TArray<FVector2D> uvs;
	TArray<FLinearColor> vertexColors;
	TArray<FProcMeshTangent> tangents;
	int vertex_count = 0;

	for (size_t i = 0; i < res; ++i)
		vertex_count += tiles[i]->generate_mesh(vertices, normals, indices, uvs, vertex_count, vertexColors);

	settings.mesh->CreateMeshSection_LinearColor(0, vertices, indices, normals, uvs, vertexColors, tangents, false);
	settings.mesh->SetMaterial(0, settings.material);
}


CityTile* CityMap::getNearesetStreetTile(CityTile* tile) const
{
	std::queue<CityTile*> queue;
	std::unordered_set<CityTile*> visited;
	queue.push(tile);
	visited.insert(tile);

	while (queue.size() > 0)
	{
		tile = queue.front();
		queue.pop();

		if (tile->getTileType() != TileType::Street)
		{
			const std::vector<CityTile*>& neighbors = tile->getNeighbors();
			for (const auto& nb : neighbors)
			{
				if (visited.find(nb) == visited.end())
				{
					queue.push(nb);
					visited.insert(nb);
				}
			}
		}
		else {
			return tile;
		}
	}

	return nullptr;
}

CityTile* CityMap::getNearesetFoodTile(CityTile* tile) const
{
	std::queue<CityTile*> queue;
	std::unordered_set<CityTile*> visited;
	queue.push(tile);
	visited.insert(tile);


	while (queue.size() > 0)
	{
		tile = queue.front();
		queue.pop();

		if (tile->getTileType() != TileType::Food)
		{
			const std::vector<CityTile*>& neighbors = tile->getNeighbors();
			for (const auto& nb : neighbors)
			{
				if (visited.find(nb) == visited.end())
				{
					queue.push(nb);
					visited.insert(nb);
				}
			}
		}
		else {
			return tile;
		}
	}

	return nullptr;
}



void CityMap::emptyOccupants()
{
	for (size_t i = 0; i < res; ++i)
		tiles[i]->emptyOccupants();
}


void CityMap::getOccupantsForPosition(std::vector<Agent*>& occupants, const FVector& pos, float radius)
{
	std::queue<CityTile*> queue;
	std::unordered_set<CityTile*> visited;
	
	float radius2 = radius * radius;
	CityTile* tile = getTileForLocation(pos);
	queue.push(tile);
	visited.insert(tile);

	if (!tile->areAgentsActive())
		return;

	while (queue.size() > 0)
	{
		tile = queue.front();
		queue.pop();
		tile->getOccupants(occupants);

		const std::vector<CityTile*>& neighbors = tile->getNeighbors();

		for (const auto& nb : neighbors)
		{
			if (nb->areAgentsActive())
				if (nb->getVecToClosestTilePoint(pos).SizeSquared() < radius2)
					if (visited.find(nb) == visited.end())
					{
						queue.push(nb);
						visited.insert(nb);
					}
		}
	}
}

FVector CityMap::wallsAvoidanceForce(const FVector& pos, const FVector& dir, const float agentRadius, const float epsilon)
{
	CityTile* tile = getTileForLocation(pos);

	const std::vector<CityTile*>& neighbors = tile->getNeighbors();
	FVector force = FVector(0), wallDir, normal;
	float dist, distAttr;

	for (const auto& nb : neighbors)
	{
		if (nb->getTileType() == TileType::Street)
			continue;

		wallDir = nb->getVecToClosestTilePoint(pos);

		dist = wallDir.Size();
		normal = nb->nearestWallNormal(pos);
		
		distAttr = agentRadius + epsilon - dist;

		if (distAttr > 0)
			force += FVector::CrossProduct(FVector::CrossProduct(normal, dir), normal).GetSafeNormal();
	}

	return force;
}

FVector CityMap::wallsRepulsionForce(const FVector& pos, const float agentRadius, const float epsilon)
{
	CityTile* tile = getTileForLocation(pos);

	const std::vector<CityTile*>& neighbors = tile->getNeighbors();
	
	FVector wallDir, normal;
	FVector force = FVector(0);
	float dist, distAttr;

	for (const auto& nb : neighbors)
	{
		if (nb->getTileType() == TileType::Street)
			continue;
		
		wallDir = nb->getVecToClosestTilePoint(pos);

		dist = wallDir.Size();
		normal = nb->nearestWallNormal(pos);

		distAttr = agentRadius + epsilon - dist;
		if(distAttr > 0)
			force += (normal * distAttr) / dist;
	}

	return force;
}