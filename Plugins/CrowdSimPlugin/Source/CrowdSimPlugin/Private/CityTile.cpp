// Fill out your copyright notice in the Description page of Project Settings.


#include "CityTile.h"

CityTile::CityTile(int _x, int _y, int _w, int _h, int _z)
	:x(_x), y(_y), pos_x(_x* _w), pos_y(_y* _h), w(_w), h(_h), z(_z) {}

FVector CityTile::getLocation() const
{
	return FVector(pos_x + w / 2, pos_y + h / 2, z + w / 10);
}

void CityTile::setup_neighbors(std::vector<CityTile*> tiles, int32 resolution_x, int32 resolution_y)
{
	//global index
	int32 idx = y * resolution_x + x;
	int32 nidx = idx - resolution_x; //init to top

	bool top = (y == 0);
	bool right = (x == (resolution_x - 1));
	bool bottom = (y == (resolution_y - 1));
	bool left = (x == 0);

	//line above
	if (!top)
		neighbors.push_back(tiles[nidx]);

	//current line
	nidx += resolution_x;
	if (!left)
		neighbors.push_back(tiles[nidx - 1]);

	if (!right)
		neighbors.push_back(tiles[nidx + 1]);

	//line bellow
	nidx += resolution_x;
	if (!bottom)
		neighbors.push_back(tiles[nidx]);
}

const std::vector<CityTile*>& CityTile::getNeighbors() const
{
	return neighbors;
}

void CityTile::emptyOccupants()
{
	occupants.clear();
}

void CityTile::addOccupant(Agent* agent)
{
	occupants.push_back(agent);
}

void CityTile::getOccupants(std::vector<Agent*>& agents) const
{
	for (auto o : occupants)
		agents.push_back(o);
}

bool CityTile::areAgentsActive() const
{
	return false;
}

FVector CityTile::getVecToClosestTilePoint(const FVector& pos)
{
	float dx = std::min(pos.X - pos_x, 0.0f) + std::max(0.0f, pos.X - (pos_x + w));
	float dy = std::min(pos.Y - pos_y, 0.0f) + std::max(0.0f, pos.Y - (pos_y + h));

	return FVector(dx, dy, 0);
}

FVector CityTile::nearestWallNormal(const FVector& pos) const
{
	return FVector(0);
}

int CityTile::generateBox(TArray<FVector>& vertices, TArray<FVector>& normals, TArray<int32>& indices, TArray<FVector2D>& uvs, const int index_offset, CityTile& tile)
{
	FVector v[8] = {
	FVector(tile.pos_x, tile.pos_y, 0),
	FVector(tile.pos_x + tile.w, tile.pos_y, 0),
	FVector(tile.pos_x + tile.w, tile.pos_y + tile.h, 0),
	FVector(tile.pos_x, tile.pos_y + tile.h, 0),
	FVector(tile.pos_x, tile.pos_y, tile.z),
	FVector(tile.pos_x, tile.pos_y + tile.h, tile.z),
	FVector(tile.pos_x + tile.w, tile.pos_y + tile.h, tile.z),
	FVector(tile.pos_x + tile.w, tile.pos_y, tile.z)
	};

	FVector n[6] = {
		FVector(0, 0, 1),
		FVector(0, 0, -1),
		FVector(0, 1, 0),
		FVector(0, -1, 0),
		FVector(0, 0, 0),
		FVector(-1, 0, 0)
	};

	FVector2D u[4] = {
		FVector2D(0, 0),
		FVector2D(1, 0),
		FVector2D(1, 1),
		FVector2D(0, 1)
	};

	//bottom side
	vertices.Add(v[0]);
	vertices.Add(v[1]);
	vertices.Add(v[2]);
	vertices.Add(v[3]);
	normals.Add(n[1]);
	normals.Add(n[1]);
	normals.Add(n[1]);
	normals.Add(n[1]);
	uvs.Add(u[0]);
	uvs.Add(u[1]);
	uvs.Add(u[2]);
	uvs.Add(u[3]);

	//top side
	vertices.Add(v[4]);
	vertices.Add(v[5]);
	vertices.Add(v[6]);
	vertices.Add(v[7]);
	normals.Add(n[0]);
	normals.Add(n[0]);
	normals.Add(n[0]);
	normals.Add(n[0]);
	uvs.Add(u[0]);
	uvs.Add(u[1]);
	uvs.Add(u[2]);
	uvs.Add(u[3]);

	//front side
	vertices.Add(v[0]);
	vertices.Add(v[3]);
	vertices.Add(v[5]);
	vertices.Add(v[4]);
	normals.Add(n[5]);
	normals.Add(n[5]);
	normals.Add(n[5]);
	normals.Add(n[5]);
	uvs.Add(u[0]);
	uvs.Add(u[1]);
	uvs.Add(u[2]);
	uvs.Add(u[3]);

	//back side
	vertices.Add(v[2]);
	vertices.Add(v[1]);
	vertices.Add(v[7]);
	vertices.Add(v[6]);
	normals.Add(n[4]);
	normals.Add(n[4]);
	normals.Add(n[4]);
	normals.Add(n[4]);
	uvs.Add(u[0]);
	uvs.Add(u[1]);
	uvs.Add(u[2]);
	uvs.Add(u[3]);

	//left side
	vertices.Add(v[1]);
	vertices.Add(v[0]);
	vertices.Add(v[4]);
	vertices.Add(v[7]);
	normals.Add(n[3]);
	normals.Add(n[3]);
	normals.Add(n[3]);
	normals.Add(n[3]);
	uvs.Add(u[0]);
	uvs.Add(u[1]);
	uvs.Add(u[2]);
	uvs.Add(u[3]);

	//right side
	vertices.Add(v[3]);
	vertices.Add(v[2]);
	vertices.Add(v[6]);
	vertices.Add(v[5]);
	normals.Add(n[2]);
	normals.Add(n[2]);
	normals.Add(n[2]);
	normals.Add(n[2]);
	uvs.Add(u[0]);
	uvs.Add(u[1]);
	uvs.Add(u[2]);
	uvs.Add(u[3]);


	for (int side = 0; side < 6; side++)
	{
		indices.Add(0 + index_offset + side * 4);
		indices.Add(1 + index_offset + side * 4);
		indices.Add(2 + index_offset + side * 4);
		indices.Add(0 + index_offset + side * 4);
		indices.Add(2 + index_offset + side * 4);
		indices.Add(3 + index_offset + side * 4);
	}

	return 6 * 4;
}