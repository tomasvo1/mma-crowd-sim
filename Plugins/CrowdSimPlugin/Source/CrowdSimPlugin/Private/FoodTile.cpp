// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodTile.h"

FoodTile::FoodTile(int _x, int _y, int _w, int _h, int _z, int _agentsLimit, int _agentsInRow)
	: BuildingTile(_x, _y, _w, _h, _z), 
	customersLimit(_agentsLimit), customersPresent(0), displayAgentRow(_agentsInRow)
{
	for (int i = 0; i < customersLimit; ++i)
		customers.push_back(nullptr);
}

int FoodTile::generate_mesh(TArray<FVector>& vertices, TArray<FVector>& normals, TArray<int32>& indices, TArray<FVector2D>& uvs, const int index_offset, TArray<FLinearColor>& color)
{
	int count = generateBox(vertices, normals, indices, uvs, index_offset, *this);

	for (int i = 0; i < count; ++i)
		color.Add(FLinearColor(0.0, 0.45, 0.11, 1.0));

	return count;
}

FVector FoodTile::compute_force(const FVector location) const
{
	float dx = std::min(location.X - pos_x, 0.0f) + std::max(0.0f, location.X - (pos_x + w));
	float dy = std::min(location.Y - pos_y, 0.0f) + std::max(0.0f, location.Y - (pos_y + h));


	//solve corners later
	if (dx != 0 && dy != 0)
		dx = dy = 0;

	if (std::abs(dx) > (w / 2.0f))
		dx = 0;

	if (std::abs(dy) > (h / 2.0f))
		dy = 0;

	if (dx != 0)
		dx = std::min(1.0f / dx, 1.0f);

	if (dy != 0)
		dy = std::min(1.0f / dy, 1.0f);

	return FVector(dx, dy, 0);
}

int FoodTile::getTileType()
{
	return TileType::Food;
}

bool FoodTile::tryAgentGetInside()
{
	if (customersLimit > customersPresent)
	{
		customersPresent++;
		return true;
	}

	return false;
}
void FoodTile::agentGetOut(const Agent* agent)
{
	customersPresent--;

	for (int i = 0; i < customersLimit; ++i)
		if (customers[i] == agent)
		{
			customers[i] = nullptr;
			break;
		}
}

FVector FoodTile::getInsideAgentPosition(const Agent* agent)
{
	FVector pos;
	int32 a, b, c;
	const float border = 2;

	int idx = 0;

	for (int i = 0; i < customersLimit; ++i)
		if (customers[i] == nullptr)
		{
			idx = i;
			customers[i] = agent;
			break;
		}

	a = idx % displayAgentRow;
	b = (idx / displayAgentRow) % displayAgentRow;
	c = idx / (displayAgentRow * displayAgentRow);

	pos = FVector(pos_x, pos_y, z);

	float spaceBetweenX = (((float)(w - 2 * border)) / (float) displayAgentRow);
	float spaceBetweenY = (((float)(h - 2 * border)) / (float) displayAgentRow);

	pos += FVector(border + (a + 0.5) * spaceBetweenX,
		border + (b + 0.5) * spaceBetweenY,
		w / 10 + c * spaceBetweenX);

	return pos;
}