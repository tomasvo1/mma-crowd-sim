// Fill out your copyright notice in the Description page of Project Settings.


#include "StreetTile.h"

StreetTile::StreetTile(int _x, int _y, int _w, int _h)
	: CityTile(_x, _y, _w, _h, 1) {}

int StreetTile::generate_mesh(TArray<FVector>& vertices, TArray<FVector>& normals, TArray<int32>& indices, TArray<FVector2D>& uvs, const int index_offset, TArray<FLinearColor>& color)
{
	int count = generateBox(vertices, normals, indices, uvs, index_offset, *this);

	for (int i = 0; i < count; ++i)
		color.Add(FLinearColor(1.0, 1.0, 1.0, 1.0));

	return count;
}

FVector StreetTile::compute_force(const FVector location) const
{
	return FVector(0);
}

int StreetTile::getTileType()
{
	return TileType::Street;
}

bool StreetTile::areAgentsActive() const
{
	return true;
}