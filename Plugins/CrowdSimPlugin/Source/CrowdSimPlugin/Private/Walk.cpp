// Fill out your copyright notice in the Description page of Project Settings.

#include "Walk.h"
#include "CityMap.h"

#include <unordered_map>
#include <vector>
#include <queue>
#include <iostream>


Walk::Walk() {}

bool collinear(FVector a, FVector b, FVector c) {
	return fabs((a.Y - b.Y) * (a.X - c.X) - (a.Y - c.Y) * (a.X - b.X)) <= 1e-9;
}

Walk::Walk(const FVector start, const FVector end, const CityMap* map, const float _tolerance)
{
	tolerance2 = _tolerance * _tolerance;
	std::unordered_map<const CityTile*, const CityTile*> pred;
	
	const CityTile * start_tile = map->getTileForLocation(start);
	const CityTile * end_tile = map->getTileForLocation(end);

	//bfs to find trajectory
	bfs(pred, start_tile, end_tile);
	auto backtrack = pred.find(end_tile);

	if (backtrack == pred.end())
	{
		return; // nothing to be done, there is not path
	}

	checkpoints.push_back(end);

	FVector origin = end;
	FVector checkpoint;
	int32 axis = -1;

	//setup trajectory checkpoints
	while (backtrack->second != nullptr)
	{
		checkpoint = backtrack->second->getLocation();
		
		if (checkpoints.size() == 1)
		{
			checkpoints.push_back(checkpoint);
		}
		else if(collinear(origin, checkpoints.back(), checkpoint))
		{
			checkpoints.back() = checkpoint;
		}
		else {
			origin = checkpoints.back();
			checkpoints.push_back(checkpoint);
		}

		backtrack = pred.find(backtrack->second);
	}

}


Walk::Walk(const FVector start, const FVector end, const float _tolerance)
{
	checkpoints.push_back(end);
	checkpoints.push_back(start);
	tolerance2 = _tolerance * _tolerance;
}

void Walk::bfs(std::unordered_map<const CityTile*, const CityTile*> & pred,
	const CityTile* start_tile, 
	const CityTile* end_tile)
{
	std::queue<const CityTile*> queue;
	const CityTile* tile = start_tile;
	queue.push(tile);
	pred[tile] = nullptr;

	while (queue.size() > 0)
	{
		tile = queue.front();
		queue.pop();

		const std::vector<CityTile*>& neighbors = tile->getNeighbors();

		//UE_LOG(LogTemp, Warning, TEXT("tile, %d %d"), tile->x, tile->y);

		for (const auto nb : neighbors)
		{

			if (nb->getTileType() != TileType::Street)
				continue;

			//UE_LOG(LogTemp, Warning, TEXT("nghb, %d %d"), nb->x, nb->y);

			if (pred.find(nb) == pred.end())
			{
				pred[nb] = tile;
				queue.push(nb);

				if (end_tile == nb)
				{
					//UE_LOG(LogTemp, Warning, TEXT("path found"));
					return;
				}
			}
		}
	}
}

Walk::~Walk()
{
}

int Walk::getForce(const FVector agent_loc, FVector& force)
{

	if (checkpoints.size() == 0)
	{
		force = FVector(0);
		return WalkStatus::finished;
	}

	//move to next checkpoint when the agent 
	//is in a certain distance from checkpoint
	//tolerance distance set to 2
	FVector dir = checkpoints.back() - agent_loc;
	if (dir.SizeSquared() < tolerance2)
	{
		checkpoints.pop_back();

		if (checkpoints.size() == 0)
		{
			force = FVector(0);
			return WalkStatus::finished;
		}

		dir = checkpoints.back() - agent_loc;
	}

	force = dir.GetSafeNormal();
	return WalkStatus::walking;
}

bool Walk::endReached() {
	return checkpoints.size() == 0;	
}