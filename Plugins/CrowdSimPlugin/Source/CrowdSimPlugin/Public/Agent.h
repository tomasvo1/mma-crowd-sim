// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Walk.h"
#include "CityMap.h"
#include "CitySettings.h"
#include "BuildingTile.h"
#include "FoodTile.h"


enum AgentState {
	home,
	searching,
	waiting,
	walkingAround,
	eating,
	returning,
	goingToHomeDoor
};
/**
 * 
 */
class CROWDSIMPLUGIN_API Agent
{
public:
	Agent(BuildingTile* _home, FVector wo, AgentSettings & settings);
	~Agent();

	void forcesCompute(CityMap* map, std::vector<Agent *> & others);
	void forcesApply(float deltaTime);

	FVector getPosition() const;

	float getFeatureState() const;


	FTransform transform;

private:
	void updateStatus(CityMap* map, std::vector<Agent*>& others);

	bool moveToStreet(CityMap* map);
	bool goFood(CityMap* map);
	bool goToFoodmarketDoor(CityMap* map);
	bool tryGetInsideFoodmarket(CityMap* map);
	bool waitAtFoodmarketDoor(CityMap* map);
	bool isGoingToWait(CityMap* map);
	bool newMarketDiscovered(CityMap* map, std::vector<Agent*>& others);
	bool planRandomWalk(CityMap* map);
	bool startEat(CityMap* map);
	bool eat(CityMap* map);
	bool goHome(CityMap* map);
	bool goToHomeDoor(CityMap* map);
	bool stayHome(CityMap* map);
	bool stopForAmount(const int amount);

	void checkStoppingRule();
	void updateWaitInterval();

	float dist(Agent* agent) const;
	int isNotLookingForOthers() const;
	bool isStopped() const;


	FVector otherAgentsRepulsionForce(std::vector<Agent*>& others) const;
	FVector otherAgentsAvoidanceForce(std::vector<Agent*>& others) const;


	//forces base
	FVector pos;
	FVector vel;
	FVector acc;
	
	//display
	FVector worldOffset;
	
	//forces
	FVector attractorForce; 
	FVector steering;
	FVector avoidanceWallForce;
	FVector avoidanceAgentsForce;
	FVector repulsion;
	FVector repulsionWallForce;
	FVector repulsionAgentsForce;

	//characteristics
	float size;
	float radius;
	float personalSpaceEpsilon;
	float pushingLambda;
	float walkTolerance;
	float hungryLevel;

	float foodCapacity;
	float foodHungerDelta;
	float foodEatingDelta;
	float patienceLimit;
	float patienceDelta;
	float talkingDistance;

	float weightWallRepulsion;
	float weightAgentRepulsion;
	float weightWallAvoidance;
	float weightAgentAvoidance;
	float weightSteeringForce;

	//stats
	float food;
	float patience;
	int stopCounter;
	int alpha;
	AgentState status;

	Walk walk;
	BuildingTile* homeTile;
	FoodTile* foodTile;
};
