// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CityTile.h"

/**
 * 
 */
class CROWDSIMPLUGIN_API BuildingTile : public CityTile
{
public:
	BuildingTile(int x, int y, int w, int h, int z);

	virtual int generate_mesh(TArray<FVector>& vertices, TArray<FVector>& normals, TArray<int32>& indices, TArray<FVector2D>& uvs, const int index_offset, TArray<FLinearColor>& color);

	virtual int getTileType();

	virtual FVector nearestWallNormal(const FVector & pos) const;
	FVector doorLocation(CityTile* street);
};
