// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include <vector>
#include "CoreMinimal.h"
#include "Math/Range.h"

#include "Agent.h"
#include "CityMap.h"

#include "ProceduralMeshComponent.h"
#include "GameFramework/Actor.h"
#include "CityGrid.generated.h"


UCLASS()
class CROWDSIMPLUGIN_API ACityGrid : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACityGrid();
	~ACityGrid();

	UPROPERTY(EditAnywhere, Category = "CityLayout", meta = (ClampMin = "15.0", ClampMax = "40.0", UIMin = "15.0", UIMax = "40.0"))
		float spacing;

	UPROPERTY(EditAnywhere, Category = "CityLayout", meta = (ClampMin = "15.0", ClampMax = "40.0", UIMin = "15.0", UIMax = "40.0"))
		float height;

	UPROPERTY(EditAnywhere, AssetRegistrySearchable, Category = "CityLayout")
		UTexture2D* layout;

	UPROPERTY(EditAnywhere, Category = "CityLayout")
		bool generateMesh;

	UPROPERTY(VisibleAnywhere, Category = "CityLayout")
		UMaterial* material;

	UPROPERTY(EditAnywhere, Category = "Animation", meta = (ClampMin = "1.0", ClampMax = "20.0", UIMin = "1.0", UIMax = "20.0"))
		float speed;

	UPROPERTY(EditAnywhere, Category = "FoodMarkets", meta = (ClampMin = "0.0", UIMin = "0.0"))
		int displayCustomersInRow;

	UPROPERTY(EditAnywhere, Category = "FoodMarkets", meta = (ClampMin = "0.0", UIMin = "0.0"))
		int limitCustomersInside;


	//agents individual features
	UPROPERTY(EditAnywhere, Category = "Agents Individual", meta = (ClampMin = "0.0", ClampMax = "100.0", UIMin = "0.0", UIMax = "10.0"))
		float personalSpaceRangeMin;
	UPROPERTY(EditAnywhere, Category = "Agents Individual", meta = (ClampMin = "0.0", ClampMax = "100.0", UIMin = "0.0", UIMax = "10.0"))
		float personalSpaceRangeMax;


	UPROPERTY(EditAnywhere, Category = "Agents Individual", meta = (ClampMin = "0.0", ClampMax = "10.0", UIMin = "0.0", UIMax = "10.0"))
		float pushFactorRangeMin;
	UPROPERTY(EditAnywhere, Category = "Agents Individual", meta = (ClampMin = "0.0", ClampMax = "10.0", UIMin = "0.0", UIMax = "10.0"))
		float pushFactorRangeMax;


	UPROPERTY(EditAnywhere, Category = "Agents Individual", meta = (ClampMin = "0.1", ClampMax = "2.0", UIMin = "0.1", UIMax = "2.0"))
		float foodHungerDeltaMin;
	UPROPERTY(EditAnywhere, Category = "Agents Individual", meta = (ClampMin = "0.1", ClampMax = "2.0", UIMin = "0.1", UIMax = "2.0"))
		float foodHungerDeltaMax;


	UPROPERTY(EditAnywhere, Category = "Agents Individual", meta = (ClampMin = "0.1", ClampMax = "2.0", UIMin = "0.1", UIMax = "2.0"))
		float foodEatingDeltaMin;
	UPROPERTY(EditAnywhere, Category = "Agents Individual", meta = (ClampMin = "0.1", ClampMax = "2.0", UIMin = "0.1", UIMax = "2.0"))
		float foodEatingDeltaMax;


	UPROPERTY(EditAnywhere, Category = "Agents Individual", meta = (ClampMin = "10.0", ClampMax = "1000.0", UIMin = "10.0", UIMax = "1000.0"))
		float foodCapacityMin;
	UPROPERTY(EditAnywhere, Category = "Agents Individual", meta = (ClampMin = "10.0", ClampMax = "1000.0", UIMin = "10.0", UIMax = "1000.0"))
		float foodCapacityMax;


	UPROPERTY(EditAnywhere, Category = "Agents Individual", meta = (ClampMin = "10.0", ClampMax = "1000.0", UIMin = "10.0", UIMax = "1000.0"))
		float patienceLimitMin;
	UPROPERTY(EditAnywhere, Category = "Agents Individual", meta = (ClampMin = "10.0", ClampMax = "1000.0", UIMin = "10.0", UIMax = "1000.0"))
		float patienceLimitMax;


	UPROPERTY(EditAnywhere, Category = "Agents Individual", meta = (ClampMin = "0.1", ClampMax = "2.0", UIMin = "0.1", UIMax = "2.0"))
		float patienceDeltaMin;
	UPROPERTY(EditAnywhere, Category = "Agents Individual", meta = (ClampMin = "0.1", ClampMax = "2.0", UIMin = "0.1", UIMax = "2.0"))
		float patienceDeltaMax;

	//Agent common features
	UPROPERTY(EditAnywhere, Category = "Agents Common", meta = (ClampMin = "1.0", ClampMax = "100.0", UIMin = "1.0", UIMax = "100.0"))
		float perceptionRadius;
	UPROPERTY(EditAnywhere, Category = "Agents Common", meta = (ClampMin = "0.0", ClampMax = "20.0", UIMin = "0.0", UIMax = "10.0"))
		int walkCheckpointTolerance;
	UPROPERTY(EditAnywhere, Category = "Agents Common", meta = (ClampMin = "0.0", ClampMax = "20.0", UIMin = "0.0", UIMax = "10.0"))
		int agentRadius;
	UPROPERTY(EditAnywhere, Category = "Agents Common", meta = (ClampMin = "1.0", ClampMax = "10.0", UIMin = "1.0", UIMax = "10.0"))
		float talkingDistance;
	UPROPERTY(EditAnywhere, Category = "Agents Common", meta = (ClampMin = "0.1", ClampMax = "10.0", UIMin = "0.1", UIMax = "10.0"))
	float weightWallRepulsion;
	UPROPERTY(EditAnywhere, Category = "Agents Common", meta = (ClampMin = "0.1", ClampMax = "10.0", UIMin = "0.1", UIMax = "10.0"))
	float weightAgentRepulsion;
	UPROPERTY(EditAnywhere, Category = "Agents Common", meta = (ClampMin = "0.1", ClampMax = "10.0", UIMin = "0.1", UIMax = "10.0"))
	float weightWallAvoidance;
	UPROPERTY(EditAnywhere, Category = "Agents Common", meta = (ClampMin = "0.1", ClampMax = "10.0", UIMin = "0.1", UIMax = "10.0"))
	float weightAgentAvoidance;
	UPROPERTY(EditAnywhere, Category = "Agents Common", meta = (ClampMin = "0.1", ClampMax = "100.0", UIMin = "0.1", UIMax = "100.0"))
	float weightSteeringForce;
	UPROPERTY(EditAnywhere, Category = "Agents Common", meta = (ClampMin = "0.1", ClampMax = "100.0", UIMin = "0.1", UIMax = "100.0"))
	float hungryLevel;

	//UPROPERTY(VisibleAnywhere, Category = "Agents Common")
		UMaterial* agentMaterial;

	//UPROPERTY(VisibleAnywhere, Category = "Agents Common")
	UStaticMesh* agentMeshInstance;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//creates mesh
	virtual void OnConstruction(const FTransform& Transform) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//init agents and city
	void initCity();

	//init agents
	void initAgents();

	//city grid mesh
	UProceduralMeshComponent* mesh;

	//instanced agent meshes
	UPROPERTY(EditAnywhere, Category = "Agents Common")
	UInstancedStaticMeshComponent* agentMesh;

	//city grid container
	CityMap* map;
	
	//simulation agents
	std::vector<Agent*> agents;
};
