// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include <vector>
#include <random>

#include "CoreMinimal.h"
#include "CityTile.h"
#include "CitySettings.h"
#include "ProceduralMeshComponent.h"

/**
 * 
 */
class CROWDSIMPLUGIN_API CityMap
{
public:
	CityMap();
	~CityMap();
	
	//generate city tiles and mesh according to supplied texture
	void generate(const CitySettings & settings);
	
	void getStreetLocations(std::vector<FVector> & locations) const;
	void getFoodLocations(std::vector<FVector> & locations) const;
	void getBuildingLocations(std::vector<FVector> & locations) const;
	const std::vector<CityTile*>& getBuildings() const;
	
	CityTile* getTileForLocation(const FVector loc) const;
	CityTile* getRandomStreetTile();
	CityTile* getNearesetStreetTile(CityTile* tile) const;
	CityTile* getNearesetFoodTile(CityTile* tile) const;

	void emptyOccupants();
	void getOccupantsForPosition(std::vector<Agent*>& occupants, const FVector & pos, float radius);

	FVector wallsAvoidanceForce(const FVector& pos, const FVector& dir, const float agentRadius, const float epsilon);
	FVector wallsRepulsionForce(const FVector& pos, const float radius, const float epsilon);

	int getRandomWaitInterval();

private:
	void generateCity(const CitySettings& settings);
	void generateMesh(const CitySettings& settings);

	int32 resolution_x;
	int32 resolution_y;
	int32 res;
	int32 spacing;
	int32 height;


	std::vector<CityTile*> tiles;
	std::vector<CityTile*> building;
	std::vector<CityTile*> food;
	std::vector<CityTile*> street;

	std::default_random_engine generator;
	std::uniform_int_distribution<int> randomStreet;
	std::uniform_int_distribution<int> randomWaitInterval;
};
