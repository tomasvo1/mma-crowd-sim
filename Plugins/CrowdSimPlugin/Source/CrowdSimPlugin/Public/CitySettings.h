// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProceduralMeshComponent.h"

/**
 * 
 */
class CROWDSIMPLUGIN_API CitySettings
{
public:
	UProceduralMeshComponent* mesh;
	UTexture2D* layout;
	UMaterial* material;
	int32 spacing;
	int32 height;
	int32 foodmarketLimit;
	int32 foodmarketAgentsRow;
};

class CROWDSIMPLUGIN_API AgentSettings
{
public:
	float size;
	float personalSpace;
	float pushing;
	float walkCheckpointTolerance;
	float radius;
	float hungryLevel;

	float foodCapacity;
	float foodHungerDelta;
	float foodEatingDelta;
	float patienceLimit;
	float patienceDelta;
	float talkingDistance;

	float weightWallRepulsion;
	float weightAgentRepulsion;
	float weightWallAvoidance;
	float weightAgentAvoidance;
	float weightSteeringForce;
};