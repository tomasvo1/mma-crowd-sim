// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include <vector>
#include <random>

#include "CoreMinimal.h"


class CROWDSIMPLUGIN_API Walk;
class CROWDSIMPLUGIN_API Agent;

enum TileType {
	Border,
	Building,
	Street,
	Food
};

class CROWDSIMPLUGIN_API Agent;

/**
 * 
 */
class CROWDSIMPLUGIN_API CityTile
{
public:
	CityTile(int x, int y, int w, int h, int z);
	virtual ~CityTile() {};

	FVector getLocation() const;

	//returns number of vertices generated
	virtual int generate_mesh(TArray<FVector>& vertices, TArray<FVector>& normals, TArray<int32>& indices, TArray<FVector2D>& uvs, const int index_offset, TArray<FLinearColor>& color) = 0;


	virtual FVector nearestWallNormal(const FVector& pos) const;
	const std::vector<CityTile*>& getNeighbors() const;


	void setup_neighbors(std::vector<CityTile*> tiles, int32 resolution_x, int32 resolution_y);

	void emptyOccupants();
	void addOccupant(Agent* agent);
	void getOccupants(std::vector<Agent*>& agents) const;

	FVector getVecToClosestTilePoint(const FVector& pos);
	virtual bool areAgentsActive() const;


	virtual int getTileType() = 0;
protected:
	static int generateBox(TArray<FVector>& vertices, TArray<FVector>& normals, TArray<int32>& indices, TArray<FVector2D>& uvs, const int index_offset, CityTile& tile);

	friend class CROWDSIMPLUGIN_API Walk;

	int x, y, pos_x, pos_y, w, h, z;

	std::vector<CityTile*> neighbors;
	std::vector<Agent*> occupants;
};


