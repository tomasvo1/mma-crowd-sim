// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BuildingTile.h"

/**
 * 
 */
class CROWDSIMPLUGIN_API FoodTile : public BuildingTile
{
public:
	FoodTile(int x, int y, int w, int h, int z, int agentsLimit, int agentsInRow);

	virtual int generate_mesh(TArray<FVector>& vertices, TArray<FVector>& normals, TArray<int32>& indices, TArray<FVector2D>& uvs, const int index_offset, TArray<FLinearColor>& color);

	virtual FVector compute_force(const FVector location) const;

	virtual int getTileType();

	bool tryAgentGetInside();
	void agentGetOut(const Agent* agent);

	FVector getInsideAgentPosition(const Agent* agent);

protected:
	int32 customersLimit;
	int32 customersPresent;
	int32 displayAgentRow;

	std::vector<const Agent *> customers;
};
