// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CityTile.h"
#include "FoodTile.h"

/**
 * 
 */
class CROWDSIMPLUGIN_API StreetTile : public CityTile
{
public:
	StreetTile(int x, int y, int w, int h);

	virtual int generate_mesh(TArray<FVector>& vertices, TArray<FVector>& normals, TArray<int32>& indices, TArray<FVector2D>& uvs, const int index_offset, TArray<FLinearColor>& color);

	virtual FVector compute_force(const FVector location) const;
	virtual int getTileType();

	virtual bool areAgentsActive() const;
};
