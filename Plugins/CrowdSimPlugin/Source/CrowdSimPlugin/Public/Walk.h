// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CityMap.h"
#include <vector>
#include <unordered_map>


enum WalkStatus {
	walking,
	finished
};

/**
 * 
 */
class CROWDSIMPLUGIN_API Walk
{
public:
	Walk();
	Walk(const FVector start, const FVector end, const CityMap * map, const float tolerance);
	Walk(const FVector start, const FVector end, const float tolerance);
	~Walk();

	int getForce(const FVector agent_loc, FVector & force);
	bool endReached();

protected:

	void bfs(std::unordered_map<const CityTile*, const CityTile*>& pred,
		const CityTile* start_tile, const CityTile* end_tile);

	std::vector<FVector> checkpoints;
	float tolerance2;

};
