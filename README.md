# How to use `CrowdSimPlugin`
In order to use the `CrowdSimPlugin`, please follow the steps below:

* Download the zip with source files
* If you already have `Plugins` folder in your project folder, copy folder `CrowdSimPlugin` inside it; in case your project misses the `Plugins` folder, create it and then copy the `CrowdSimPlugin` inside it
* Enable the plugin in your main project `*.uproject` file with:

        ...
    	"Plugins": [
		{
			"Name": "CrowdSimPlugin",
			"Enabled": true
		}

* Build the plugin when you reopen your project
* In `CrowdSimPlugin C++ Classes` you should be able to find new Actor class `CityGrid`, drag it into the viewport
* The `CrowdSimPlugin` contains 5 default layout textures in its `Content` folder, pick one and select it in the `Layout` parameter
* Click `GenerateMesh`, the geometry should be visible
* Position your character starting point, setup simulation parameters, and you are good to go

This manual is also part of the report.
